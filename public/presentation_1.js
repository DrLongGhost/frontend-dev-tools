function loadWidgets() {
    window.container = [];

    $('a.trackable').each(function(idx, elem) {
        var aWidget = new widget();
        aWidget.setElement(elem);
        container.push(aWidget);
    });

    container.push(new widget(document.getElementById('table1')));

    var links = [];
    _.each(container, function(obj) {
        links.push(obj.getTruncatedHref());
    });

    //console.log(links);
}

var sampleObject = {
    counter: 15,
    children: [
        { name: 'foo' },
        { name: 'bar' },
        { name: 'baz' }
    ],
    doStuff: function() {
        for (var i=0; i < this.children.length; i++) {
            console.log('I found ' + this.children[i].name);
        }
        return true;
    }
};

addElem = () => {
  let item = document.createElement('div');
  item.id = 'newlyAdded';
  item.innerHTML = '<div id="onhoverDiv" style="background-color: #AAAAAA; width: 400px; height: 200px;">Now You See Me...</div>';
  document.getElementById('listToAppend').appendChild(item);
}
removeElem = () => {
  let item = document.getElementById('newlyAdded');
  document.getElementById('listToAppend').removeChild(item);
}

$(function() {
    $('a').each(function(idx, elem) {
        if (this.href) {
            this.target = '_blank';
        }
    });

    $('#submit2').click(function(){
       var name = 'Charles Norris',
           email = 'c.norris@certified-badass.com',
           phone = '215-867-5309';
       $.ajax({
          type: "POST",
          url: "presentations",
          data: { 'name': name, 'email':email, 'phone': phone },
          success: function() { alert('Data was posted'); }
       })
    });
});
