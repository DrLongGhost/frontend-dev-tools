/**
 * Widget Class for instantiating a widget
 * @author Dan D
 */
function widget() {
    this.element = window.document;

    this.getElement = function getElement() {
        return this.element;
    }

    this.setElement = function setElement(val) {
        this.element = val;
    }

    this.getTruncatedHref = function() {
        var elem = this.getElement(), shortened;

        shortened = elem.href.substring(0,100);
        return shortened;
    }
}


